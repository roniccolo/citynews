import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { LoginDialogComponent } from 'src/app/components/auth/login-dialog/login-dialog.component';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {

  isLoggedIn = false;
  loggedInUser;

  constructor(
    private userService: UserService,
    private router: Router,
    public dialog: MatDialog) { }

  ngOnInit() {

    this.userService.isLoggedInListener.asObservable().subscribe(status => {
      this.isLoggedIn = status;
    });

    this.userService.userInfoListener.asObservable().subscribe(user => {
      this.loggedInUser = user;
    });

    if (localStorage.getItem('userInfo')) {
      this.loggedInUser = JSON.parse(localStorage.getItem('userInfo'));
      this.isLoggedIn = true;
    }


  }

  onLogout() {
    this.userService.logOut();
    this.router.navigate(['/']);
  }



  loginDialog(): void {
    const dialogRef = this.dialog.open(LoginDialogComponent);
    // dialogRef.afterClosed().subscribe(() => {
    //   console.log('no info to return at the moment')
    // });
  }



}
