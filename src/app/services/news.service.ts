import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class NewsService {

  baseUrl = 'http://localhost:3000/news/';
  apiNewsUrl = 'http://newsapi.org/v2/top-headlines?' +
          'sources=bbc-news&' +
          'apiKey=cedce8a4532a4a60919679aeadce77d5';
  romanianApiNews = 'http://newsapi.org/v2/top-headlines?country=ro&apiKey=cedce8a4532a4a60919679aeadce77d5';

  constructor(private http: HttpClient, private router: Router) { }

  getAllApiNews() {
    return this.http.get<any>(this.apiNewsUrl);
  }

  getRomanianApiNews() {
    return this.http.get<any>(this.romanianApiNews);
  }


}

