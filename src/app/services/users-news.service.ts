import { Injectable } from '@angular/core';
import { HttpClient, HttpEventType } from '@angular/common/http';
import { News } from '../models/news';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})
export class UsersNewsService {

  baseUrl = 'http://localhost:3000/news/';
  userAddedNews: News[];
  userNewsListener: BehaviorSubject<News[]> = new BehaviorSubject<News[]>([]);
  specificNewsListener: BehaviorSubject<any> = new BehaviorSubject<any>('');

  constructor(
    private http: HttpClient,
    private router: Router,
    public snackBar: MatSnackBar) { }

  getUsersAddedNews() {
    this.http.get<any>(this.baseUrl).subscribe((response => {
      this.userAddedNews = response.news;
      this.userNewsListener.next([...this.userAddedNews]);
    }));
  }

  getUserNewsDetails(id: string) {
    return this.http.get<News>(this.baseUrl + id);
  }

  getEditedNews(newsId, updatedNews) {
    let newsToBeUpdated: News | FormData;      // !!! or we send a FORMDATA or a JSON
    if (typeof (updatedNews.image) === 'object') {
      newsToBeUpdated = new FormData();    // if the news added has an image we send to BE a FormData()
      newsToBeUpdated.append('title', updatedNews.title);
      newsToBeUpdated.append('content', updatedNews.content);
      newsToBeUpdated.append('category', updatedNews.category);
      newsToBeUpdated.append('allowComments', updatedNews.allowComments);
      newsToBeUpdated.append('image', updatedNews.image);
    } else {
      newsToBeUpdated = updatedNews;     // if the news added doesn't have an image we send to BE a JSON
    }
    this.http.put<any>(this.baseUrl + newsId, newsToBeUpdated).subscribe(() => {
      this.openSnackBar('News successfully updated!', 'OK');
      if (updatedNews.category === 'International') {
        this.router.navigate(['/city-news']);
      }
    });
  }

  postNews(news) {
    let newsData: News | FormData;     // !!! or we send a FORMDATA or a JSON
    if (typeof (news.image) === 'object') {
      newsData = new FormData();  // if the news added has an image we send to BE a FormData()
      newsData.append('title', news.title);
      newsData.append('content', news.content);
      newsData.append('category', news.category);
      newsData.append('allowComments', news.allowComments);
      newsData.append('image', news.image);
    } else { // if is a JSON (no image attached)
      newsData = news;   // if the news added doesn't have an image we send to BE a JSON
    }

    this.http.post<any>(this.baseUrl, newsData).subscribe((response) => {
      this.openSnackBar('The news was successfully added!', 'OK');
      console.log('response', response);
      if (news.category === 'International') {
        this.router.navigate(['/city-news']);
      }
    });
  }

  deleteNews(id) {
    this.openSnackBar('The news was successfully deleted!', 'OK');
    return this.http.delete(this.baseUrl + id);
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, { duration: 2000, verticalPosition: 'top' });
  }


}
