import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../models/user';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';
import { UsersNewsService } from './users-news.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  authBaseUrl = 'http://localhost:3000/user/';
  token;
  userInfo: User;
  isLoggedIn: boolean;
  isLoggedInListener: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  userInfoListener: BehaviorSubject<any> = new BehaviorSubject<any>('');


  constructor(
      private http: HttpClient,
      private router: Router,
      private usersNewsService: UsersNewsService,
      public  snackBar: MatSnackBar) { }

  loginUser(userCredentials) {
    this.http.post<any>(this.authBaseUrl + 'login', userCredentials).subscribe(loginResponse => {
        if (loginResponse) {
          // tslint:disable-next-line: no-unused-expression
          this.usersNewsService.userAddedNews;  // userului ii vor aparea optiunile de edit si delete a news-ului dupa login fara sa fie nevoit sa dea refresh la pagina
          this.openSnackBar();
          console.log('loginResponse', loginResponse);
          this.userInfo = loginResponse.userInfo;
          this.token = loginResponse.token;
          this.isLoggedIn = true;
          this.isLoggedInListener.next(true);
          this.userInfoListener.next(loginResponse.userInfo);
          localStorage.setItem('token', this.token);
          localStorage.setItem('userInfo', JSON.stringify(loginResponse.userInfo));
        }
    }, error => this.isLoggedInListener.next(false));
  }

  getUserToken() {
    return this.token;
  }

  getUserInfo() {
    return this.userInfo;
  }

  createUser(newUser) {
    this.http.post<User>(this.authBaseUrl + 'register', newUser).subscribe( () => {
        this.router.navigate(['/']);
    });
  }

  autoLogin() {
    this.token = localStorage.getItem('token');
    this.isLoggedIn = true;
    this.isLoggedInListener.next(true);
  }

  logOut() {
    localStorage.removeItem('token');
    localStorage.removeItem('userInfo');
    this.token = null;
    this.isLoggedInListener.next(false);
  }

  openSnackBar() {
    const message = 'You are now logged in';
    const action = 'OK';
    this.snackBar.open(message, action, {duration: 3000, verticalPosition: 'top'});
  }

}
