import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BBCNewsComponent } from './components/news/bbc-news/bbc-news.component';
import { RomanianNewsComponent } from './components/news/romanian-news/romanian-news.component';
import { CityNewsComponent } from './components/news/city-news/city-news.component';
import { SignupComponent } from './components/auth/signup/signup.component';
import { AddNewsComponent } from './components/actions/add-news/add-news.component';
import { EditAddedNewsComponent } from './components/actions/editAddedNews/editAddedNews.component';
import { NewsDetailsComponent } from './components/news-details/news-details.component';
import { LoginDialogComponent } from './components/auth/login-dialog/login-dialog.component';


const routes: Routes = [
  { path: '', component: BBCNewsComponent, data: { title: 'BBC News' } },
  { path: 'romanian-news', component: RomanianNewsComponent, data: { title: 'National News' } },
  { path: 'city-news', component: CityNewsComponent, data: { title: 'City News' } },
  { path: 'edit-news/:id', component: EditAddedNewsComponent, data: { title: 'City News Details' } },
  { path: 'news-details/:id', component: NewsDetailsComponent, data: { title: 'News Details' } },
  { path: 'login', component: LoginDialogComponent, data: { title: 'login' } },
  { path: 'signup', component: SignupComponent, data: { title: 'signup' } },
  { path: 'add-news', component: AddNewsComponent, data: { title: 'add-news' } }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
