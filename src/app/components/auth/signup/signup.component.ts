import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserService } from 'src/app/services/user.service';


@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  form: FormGroup;
  hide = true;

  constructor(private userService: UserService) { }

  ngOnInit() {
    this.form = new FormGroup({
      name: new FormControl('', { validators: [Validators.required, Validators.minLength(3)] }),
      profession: new FormControl('', { validators: [Validators.required] }),
      email: new FormControl('', { validators: [Validators.required, Validators.minLength(3)] }),
      password: new FormControl('', { validators: [Validators.required, Validators.minLength(3)] })
    });
  }

  onRegister(form) {
    this.userService.createUser(form.value);
  }

}
