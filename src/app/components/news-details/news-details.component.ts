import { Component, OnInit } from '@angular/core';
import { UsersNewsService } from 'src/app/services/users-news.service';
import { News } from 'src/app/models/news';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
import { ConfirmDialogModel, ConfirmationDialogComponent } from '../../layout/confirmation-dialog/confirmation-dialog.component';

import { User } from '../../models/user';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-news-details',
  templateUrl: './news-details.component.html',
  styleUrls: ['./news-details.component.css']
})
export class NewsDetailsComponent implements OnInit {
  isLoading = true;
  isLoggIn = false;
  writeComment = false;
  specificNews: News;
  specificNewsId: string;
  userInfo: User;
  userComment = '';

  constructor(
    private usersNewsService: UsersNewsService,
    private userService: UserService,
    private route: ActivatedRoute,
    public dialog: MatDialog,
    private router: Router) { }

  ngOnInit() {
    this.route.paramMap.subscribe((paramMap: ParamMap) => {
      if (paramMap.has('id')) {
        this.specificNewsId = paramMap.get('id');
      }
    });

    this.usersNewsService.getUserNewsDetails(this.specificNewsId).subscribe(thenews => this.specificNews = thenews);

    this.userService.isLoggedInListener.asObservable().subscribe(loginStatus => this.isLoggIn = loginStatus);
    // this.userService.userInfoListener.asObservable().subscribe(user => this.userInfo = user);  // userInfo lose info after refresh!?!
    if (localStorage.getItem('userInfo')) {
      this.userInfo = JSON.parse(localStorage.getItem('userInfo'));
    }
  }

  confirmDialog(id): void {
    const title = 'Confirmation Required';
    const message = `${this.userInfo.name}, are you sure you want to delete the news?`;
    const dialogData = new ConfirmDialogModel(title, message);  // conform constructorului din ConfirmDialogModel
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, { data: dialogData });
    this.isLoading = true;
    dialogRef.afterClosed().subscribe(dialogResult => {
      if (dialogResult) {
        this.usersNewsService.deleteNews(id).subscribe(() => {
          this.isLoading = false;
          this.usersNewsService.getUsersAddedNews();
          this.isLoading = false;
          this.router.navigate(['/city-news']);
        });
      } else {
        this.isLoading = false;
      }
    });
  }

  onWriteComment() {
    this.writeComment = !this.writeComment;
  }

  onSubmitComment() {
    console.log('userComment', this.userComment);
  }





}
