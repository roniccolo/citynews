import { Component, OnInit } from '@angular/core';
import { UsersNewsService } from '../../../services/users-news.service';
import { News } from 'src/app/models/news';
import { User } from 'src/app/models/user';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-city-news',
  templateUrl: './city-news.component.html',
  styleUrls: ['./city-news.component.css']
})
export class CityNewsComponent implements OnInit {

  isLoading = true;
  isLoggIn = false;
  user: User;
  internationNews: News[];
  professionalInternationalNews: News[];
  amateurInternationalNews: News[];

  constructor(
    private usersNewsService: UsersNewsService,
    private userService: UserService) { }

  ngOnInit() {
    this.usersNewsService.getUsersAddedNews();
    this.usersNewsService.userNewsListener.asObservable().subscribe(response => {
      this.isLoading = false;
      this.internationNews = response.filter(item => item.category === 'International');
      this.professionalInternationalNews = response.filter(item => item.category === 'International' && item.userProfession === 'Professional Journalist');
    });
    this.usersNewsService.userNewsListener.asObservable().subscribe(response => {
      this.isLoading = false;
      this.amateurInternationalNews = response.filter(item => item.category === 'International' && item.userProfession === 'Amateur Journalist');
    });
    this.userService.isLoggedInListener.asObservable().subscribe(loginStatus => {
      this.isLoggIn = loginStatus;
    });
    if (localStorage.getItem('userInfo')) {
      this.user = JSON.parse(localStorage.getItem('userInfo'));
    }
  }

  //  get sortInternationalNewsByDate() {
  //   return this.professionalInternationalNews.sort((a, b) => {
  //     return  new Date(b.createdAt) as any -  (new Date(a.createdAt) as any);
  //   });
  // }


}
