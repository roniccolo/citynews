import { Component, OnInit } from '@angular/core';
import { NewsService } from 'src/app/services/news.service';

@Component({
  selector: 'app-romanian-news',
  templateUrl: './romanian-news.component.html',
  styleUrls: ['./romanian-news.component.css']
})
export class RomanianNewsComponent implements OnInit {

  isLoading = true;
  romanianNews;

  constructor(private newsService: NewsService) { }

  ngOnInit() {
    this.newsService.getRomanianApiNews().subscribe(response => {
      this.romanianNews = response.articles;
      this.isLoading = false;
    });
  }

}

