import { Component, OnInit } from '@angular/core';
import { NewsService } from '../../../services/news.service';

@Component({
  selector: 'app-bbc-news',
  templateUrl: './bbc-news.component.html',
  styleUrls: ['./bbc-news.component.css']
})
export class BBCNewsComponent implements OnInit {
  isLoading = true;
  allApiNews = [];

  constructor(private newsService: NewsService) { }

  ngOnInit() {

    this.newsService.getAllApiNews().subscribe(response => {
      this.allApiNews = response.articles;
      this.isLoading = false;
    });

  }
}
