import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { UsersNewsService } from '../../../services/users-news.service';

import { News } from 'src/app/models/news';


@Component({
  selector: 'app-edit-added-news',
  templateUrl: './editAddedNews.component.html',
  styleUrls: ['./editAddedNews.component.css']
})
export class EditAddedNewsComponent implements OnInit {

  newsId: string;
  theNews: News;
  form: FormGroup;
  allow: boolean;
  isLoading = true;
  imagePreview: string;

  constructor(
    private route: ActivatedRoute,
    private usersNewsService: UsersNewsService) { }

  ngOnInit() {

    this.form = new FormGroup({
      title: new FormControl(null, { validators: [Validators.required, Validators.minLength(3)] }),
      content: new FormControl(null, { validators: [Validators.required, Validators.minLength(3)] }),
      category: new FormControl(null, { validators: [Validators.required] }),
      allowComments: new FormControl(null, { validators: [Validators.required] }),
      image: new FormControl(null)
    });


    this.route.paramMap.subscribe((paramMap: ParamMap) => {
      if (paramMap.has('id')) {
        this.newsId = paramMap.get('id');
        this.isLoading = true;

        this.usersNewsService.getUserNewsDetails(this.newsId).subscribe(resData => {
          this.isLoading = false;
          this.allow = resData.allowComments;
          // get current value of the news
          this.theNews = {
            _id: resData._id,
            title: resData.title,
            content: resData.content,
            category: resData.category,
            allowComments: resData.allowComments,
            createdAt: resData.createdAt,
            userName: resData.userName,
            imagePath: resData.imagePath,
          };
          // set form with the default values
          this.form.setValue({
            title: this.theNews.title,
            content: this.theNews.content,
            category: this.theNews.category,
            allowComments: this.theNews.allowComments,
            image: this.theNews.imagePath || null
          });
        });
      }
    });


  }

  onNewsImage(event: Event) {
    const file = (event.target as HTMLInputElement).files[0];
    console.log('file:', file);
    if (file.type === 'image/jpeg' || file.type === 'image/png' || file.type === 'image/jpg') {
      // we use 'patchValue' to set one form property  || setValue(like above) to set multiple form values
      this.form.patchValue({ image: file });  // we set the form.image equal to the file (the selected image)
      this.form.get('image').updateValueAndValidity();  // we need this to update the new form value
      // we need to create a FileReader so that html can read/display out preview image !!!
      const reader = new FileReader();
      reader.onload = () => {
        this.imagePreview = reader.result as string;
      };
      reader.readAsDataURL(file);
    } else {
      console.log('Please add an image');
    }
  }


  onSaveEditNews(form) {
    this.usersNewsService.getEditedNews(this.newsId, form.value);
    // this.form.reset();
  }




}
