import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UsersNewsService } from '../../../services/users-news.service';

@Component({
  selector: 'app-add-news',
  templateUrl: './add-news.component.html',
  styleUrls: ['./add-news.component.css']
})
export class AddNewsComponent implements OnInit {

  form: FormGroup;
  imagePreview: string;


  constructor(private usersNewsService: UsersNewsService) { }

  ngOnInit() {
    this.form = new FormGroup({
      title: new FormControl(null, { validators: [Validators.required, Validators.minLength(3)] }),
      content: new FormControl(null, { validators: [Validators.required, Validators.minLength(3)] }),
      category: new FormControl(null, { validators: [Validators.required] }),
      allowComments: new FormControl(null, { validators: [Validators.required] }),
      image: new FormControl(null)
    });
  }

  onNewsImage(event: Event) {
    const file = (event.target as HTMLInputElement).files[0];
    // we use 'patchValue' to set one(or some) form property  || setValue(like in edit news) to set all form values
    if (file.type === 'image/jpeg' || file.type === 'image/png' || file.type === 'image/jpg') {
      this.form.patchValue({ image: file });  // we set the form.image equal to the file (the selected image)
      this.form.get('image').updateValueAndValidity();  // we need this to update the new form value
      // we need to create a FileReader so that html can read/display out preview image !!!
      const reader = new FileReader();
      reader.onload = () => {
        this.imagePreview = reader.result as string;
      };
      reader.readAsDataURL(file);
    } else {
      console.log('Please add an image');
    }

  }

  onAddNews(form) {
    if (this.form.invalid) {
      return;
    }
    const newsAdded = {
      title: form.value.title,
      content: form.value.content,
      category: form.value.category,
      allowComments: form.value.allowComments,
      image: this.form.value.image
    };
    this.usersNewsService.postNews(newsAdded);
  }

}
