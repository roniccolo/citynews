import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpHandler, HttpRequest, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { UserService } from '../services/user.service';

@Injectable()
export class Interceptor implements HttpInterceptor {

  constructor(private userService: UserService) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    const authToken = this.userService.getUserToken();

    const httpRequest = request.clone({
        headers: request.headers.set('Authorization', 'Bearer ' + authToken)  // 'Bearer ' with space at the end. backend middleware will have the same
    });
    return next.handle(httpRequest);
  }
}
