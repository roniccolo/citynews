import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ConfirmationDialogComponent } from './layout/confirmation-dialog/confirmation-dialog.component';

import { AngularMaterialModule } from './angular-material.module';

import { NavigationComponent } from './layout/navigation/navigation.component';
import { BBCNewsComponent } from './components/news/bbc-news/bbc-news.component';
import { RomanianNewsComponent } from './components/news/romanian-news/romanian-news.component';
import { SignupComponent } from './components/auth/signup/signup.component';
import { AddNewsComponent } from './components/actions/add-news/add-news.component';
import { FooterComponent } from './layout/footer/footer.component';
import { CityNewsComponent } from './components/news/city-news/city-news.component';
import { EditAddedNewsComponent } from './components/actions/editAddedNews/editAddedNews.component';
import { NewsDetailsComponent } from './components/news-details/news-details.component';

import { Interceptor } from './interceptors/auth-interceptor';
import { LoginDialogComponent } from './components/auth/login-dialog/login-dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    BBCNewsComponent,
    RomanianNewsComponent,
    SignupComponent,
    AddNewsComponent,
    FooterComponent,
    CityNewsComponent,
    EditAddedNewsComponent,
    NewsDetailsComponent,
    ConfirmationDialogComponent,
    LoginDialogComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    FormsModule,
    AngularMaterialModule,
    FlexLayoutModule
  ],
  providers: [{ provide: HTTP_INTERCEPTORS, useClass: Interceptor, multi: true }],
  bootstrap: [AppComponent],
  entryComponents: [ConfirmationDialogComponent, LoginDialogComponent]
})
export class AppModule { }
