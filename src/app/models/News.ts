export interface News {
  _id: string;
  title: string;
  content: string;
  category: string;
  allowComments: boolean;
  createdAt: Date;
  userName: string;
  userId?: string;
  imagePath?: string;
  imageName?: string;
  userProfession?: string;
}
