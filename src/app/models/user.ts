export interface User {
    _id: string;
    name: string;
    profession: string;
    email: string;
    password: string;
}
