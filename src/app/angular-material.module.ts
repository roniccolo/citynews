import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule, MatNativeDateModule, MatIconModule, MatSidenavModule, MatListModule, MatToolbarModule,
        MatCardModule, MatFormFieldModule, MatInputModule, MatProgressSpinnerModule, MatTabsModule,
        MatPaginatorModule, MatDialogModule, MatSelectModule, MatRadioModule, MatSnackBarModule } from '@angular/material';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MatButtonModule, MatToolbarModule, MatNativeDateModule, MatTabsModule,
    MatIconModule, MatSidenavModule, MatListModule, MatCardModule, MatSnackBarModule,
    MatInputModule, MatProgressSpinnerModule, MatPaginatorModule, MatDialogModule, MatFormFieldModule, MatSelectModule, MatRadioModule
  ],
  exports: [
    CommonModule,
    MatButtonModule, MatToolbarModule, MatNativeDateModule, MatTabsModule,
    MatIconModule, MatSidenavModule, MatListModule, MatCardModule, MatSidenavModule, MatListModule, MatSnackBarModule,
    MatInputModule, MatProgressSpinnerModule, MatPaginatorModule, MatDialogModule, MatFormFieldModule, MatSelectModule, MatRadioModule
  ]
})
export class AngularMaterialModule { }
