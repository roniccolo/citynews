require("dotenv").config();
const bcryptjs = require("bcryptjs");
const jwt = require("jsonwebtoken");

const Users = require("../models/users");

exports.createUser = (req, res, next) => {
  bcryptjs.hash(req.body.password, 10).then((hash) => {
    const user = new Users({
      name: req.body.name,
      profession: req.body.profession,
      email: req.body.email,
      password: hash,
    });

    user
      .save()
      .then((respond) => {
        res.status(200).json({
          userInfo: {
            _id: user._id,
            name: user.name,
            profession: user.profession,
            email: user.email,
          },
        });
      })
      .catch((error) => {
        res.status(401).json({
          message: "Nice try, but this email is already in use",
        });
      });
  });
};

exports.loginUser = async (req, res, next) => {
  let user = await Users.findOne({ email: req.body.email });
  console.log("user:", user);
  if (!user) {
    res.status(404).json({
      message: "There is no account with this email address",
    });
  }
  try {
    let passwordCompared = await bcryptjs.compare(
      req.body.password,
      user.password
    );
    if (!passwordCompared) {
      res.status(401).json({
        message: "Wrong password!",
      });
    }

    const tokenPayload = {
      userId: user._id,
      userName: user.name,
      userEmail: user.email,
      userProfession: user.profession,
    };

    const token = jwt.sign(
      { userInfo: tokenPayload },
      "process.env.ACCESS_TOKEN"
    );
    res.status(200).json({
      token,
      userInfo: {
        _id: user._id,
        name: user.name,
        profession: user.profession,
        email: user.email,
      },
    });
  } catch (error) {
    res.status(500).json({
      message: error.message,
    });
  }
};
