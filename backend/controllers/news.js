const News = require("../models/news");
const fs = require('fs');


exports.getAllNewss = (req, res, next) => {
  const allNews = News.find();
  allNews
    .then(news => {
      res.status(200).json({
        news: news
      });
    })
    .catch(error => {
      res.status(500).json({
        message: "Fetching news failed"
      });
    });
};

exports.getNewsDetails = (req, res, next) => {
  News.findById(req.params.id)
    .then(specificNews => {
      if (specificNews) {
        res.status(200).json(specificNews);
      } else {
        res.status(404).json({
          message: "The news was not found"
        });
      }
    })
    .catch(error => {
      res.status(500).json({
        message: "The news couldn't be fetched"
      });
    });
};

exports.addNews = (req, res, next) => { // 'image' - how it is in FormData from FE
  if (req.body) {  // IF THE REQUEST HAS THE TYPE 'body', then...
    imagePath = req.body.imagePath; // or just say null // be default we received from FE a JSON (if news is added without image, imagePath is null)
    imageName = req.body.imageName; // or just say null // same as above
  }

  if (req.file) {  // // IF THE REQUEST HAS THE TYPE 'file', (news was added with image, we received a FormData from FE)
    const url = req.protocol + '://' + req.get('host');
    imagePath = url + "/images/" + req.file.filename;  // url address where we find the image
    imageName = req.file.filename;
  }

  const news = new News({
    title: req.body.title,
    content: req.body.content,
    category: req.body.category,
    allowComments: req.body.allowComments,
    userName: req.userInfo.userName,
    userProfession: req.userInfo.userProfession,
    userId: req.userInfo.userId,
    imagePath: imagePath,
    imageName: imageName,

  });
  news.save()
    .then(createdNews => {
      console.log('news added: ', news)
      res.status(201).json({
        message: "News added successfully",
        createdNews
      });
    })
    .catch(error => {
      res.status(500).json({
        message: "Error! News not created"
      });
    });
};

exports.editNews = async (req, res, next) => {
  if (req.body) {
    imagePath = req.body.imagePath;
    imageName = req.body.imageName;
  }

  if (req.file) {
    const url = req.protocol + "://" + req.get("host");
    imagePath = url + "/images/" + req.file.filename;
    imageName = req.file.filename;
  }
  const newsToBeUpdated = await News.findById(req.params.id);
  const updatedNews = new News({
    _id: req.params.id,
    title: req.body.title,
    content: req.body.content,
    category: req.body.category,
    allowComments: req.body.allowComments,
    imagePath: imagePath,
    imageName: imageName
  });

  try {
    if (req.userInfo.userId == newsToBeUpdated.userId) { // comparing userInfo.userId from request token to be equl with userId who have created the news;

      await News.updateOne({_id: req.params.id}, updatedNews);
      if (newsToBeUpdated.imageName && newsToBeUpdated.imageName !== newsToBeUpdated.imageName) {  // delete from server the old news image if the incoming image is not sa the same
        fs.unlinkSync(`backend/images/${newsToBeUpdated.imageName}`);  // it must me async 'unlinkSync'. the 'unlink won't do it'
      }
      return res.status(200).json({
        message: "News updated successfully",
        newsUpdated: updatedNews
      });
    } else {
      return res.status(401).json({
        message: "Not authorized"
      });
    }
  } catch (error) {
    res.status(500).json({
      message: "The news could't be updated"
    });
  }
};


exports.deleteNews = async (req, res, next) => {      // a usual req.body
  const news = await News.findById(req.params.id);
  if (!news) {
    return res.status(404).json({
      message: "No news was found"
    });
  }
  try {
    if (req.userInfo.userId == news.userId) {  // comparing userInfo.userId from request token to be equal with userId who have created the news
      await news.remove();
      if (news.imageName) {  // if statement is need it, otherwise if there is no image, when fs will try to delete the image the app will crash
        fs.unlinkSync(`backend/images/${news.imageName}`);  // it must me async 'unlinkSync'. the 'unlink won't do it'
      }
      res.status(204).json({  // 204 -> 'No content'...the request was successful and we will not return any content
        message: "News deleted successfully"
      });
    } else {
      res.status(401).json({
        message: "You are not the owner of this news"
      });
    }
  } catch (err) {
    return res.status(500).json({
      error: "Server Error"
    });
  }
};
