require("dotenv").config();
const jwt = require("jsonwebtoken");

module.exports = (req, res, next) => {
  try {
    const token = req.headers.authorization.split(" ")[1];
    if (!token) {
      res.status(401).json({
        message: "There is no token provided"
      });
    }

    const decodedToken = jwt.verify(token, "process.env.ACCESS_TOKEN");
    if (!decodedToken) {
      res.status(401).json({
        message: "This token is invalid"
      });
    }
    req.userInfo = decodedToken.userInfo;
    next();
  } catch (err) {
    res.status(401).json({
      message: "You must be authenticated!"
    });
  }
};
