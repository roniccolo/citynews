const express = require("express");
const checkAuth = require("../middleware/authenticateToken");
const newsController = require("../controllers/news");
const router = express.Router();
const multer = require('multer');


// !!!! if you try to set the save destionation with '../images' or other variations IT DOESN'T WORK!!!!
const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, 'backend/images') // !!!! if you try to set the save destionation with '../images' or other variations IT DOESN'T WORK!!!!
  },
  filename: (req, file, cb) => {
    const name = file.originalname.toLocaleLowerCase().split(' ').join('-');
    cb(null, Date.now() + '-' + name); // adding Date.now() in the save image name. we will need it later to delete a specific image
  }
})

const filterImage = (req, file, cb) => {
  if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png' || file.mimetype === 'image/jpg') {
    cb(null, true);
  } else {
    cb(null, false);
  }
}

const uploadImages = multer({
  storage: storage,
  fileFilter: filterImage,
  limits: {
      fileSize: 1024 * 1024 * 5  // 1024 * 1024 = 1MB * 5 => photoImage = max 5MB
      },
});




router.post("", checkAuth, checkAuth, uploadImages.single('image'), newsController.addNews);
router.put("/:id", checkAuth, checkAuth, uploadImages.single('image'), newsController.editNews);
router.get("", newsController.getAllNewss);
router.get("/:id", newsController.getNewsDetails);
router.delete("/:id", checkAuth, newsController.deleteNews);

module.exports = router;
