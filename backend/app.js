require('dotenv').config();
const path = require('path');
const express = require('express');
const app = express();
const cors = require('cors');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const newsRoutes = require('./routes/news');
const userRoutes = require('./routes/users');

// mongoose.connect('mongodb+srv://roniccolo:1235@cluster0-qpnfe.mongodb.net/Blog?retryWrites=true&w=majority', { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true })
// mongoose.connect('mongodb+srv://roniccolo:12346@citynews-cluster-iwwly.mongodb.net/CityNews', { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true })

mongoose.connect('mongodb+srv://roniccolo:1235@cluster0-qpnfe.mongodb.net/News?retryWrites=true&w=majority', { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true })
    .then( () => {
      console.log('Mongodb connected')
    })
    .catch( () => {
      console.log('Mongodb connection failed')
    })

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use('/images', express.static(path.join('backend/images')));  // requesturile ce contin in url '/images' vor avea acces in folderul backend/images
app.use('/news', newsRoutes);
app.use('/user', userRoutes);



module.exports = app;
