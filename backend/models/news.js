const mongoose = require("mongoose");

const newsSchema = mongoose.Schema({
  title: { type: String, required: true },
  content: { type: String, required: true },
  category: { type: String, required: true },
  allowComments: { type: Boolean, required: true },
  userName: { type: String, ref: 'Users', required: true},
  userId: { type: mongoose.Schema.Types.ObjectId, ref: "Users", required: true},
  userProfession: { type: String, ref: "Users"},
  createdAt: { type: Date, default: Date.now() },
  imagePath: { type: String, required: false},
  imageName: { type: String, required: false}
});

module.exports = mongoose.model("News", newsSchema);
